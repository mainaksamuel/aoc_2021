// Day 01.
use std::str::FromStr;

pub fn larger_than_previous(input_str: String) -> u32 {
    let input = input_str
        .split("\n")
        .filter(|num| num.trim().len() > 1)
        .map(|num| i32::from_str(num.trim()).unwrap())
        .collect::<Vec<i32>>();

    let mut increases = 0;

    for window in input.windows(2) {
        if window[1] > window[0] {
            increases += 1;
        }
    }

    // for i in 1..(input.len()) {
    //     if input[i] > input[i - 1] {
    //         increases += 1;
    //     }
    // }

    increases
}

pub fn larger_than_previous_part2(input_str: String) -> u32 {
    let input = input_str
        .split("\n")
        .filter(|num| num.trim().len() > 1)
        .map(|num| i32::from_str(num.trim()).unwrap())
        .collect::<Vec<i32>>();

    let mut increases = 0;

    for window in input.windows(4) {
        if window[3] > window[0] {
            increases += 1;
        }
    }

    // let mut prev = input[0] + input[1] + input[2];
    // for i in 4..(input.len()) {
    //     let current = input[i] + input[i - 1] + input[i - 2];
    //     if current > prev {
    //         increases += 1;
    //     }
    //     prev = current;
    // }

    increases
}
