// Day 02.

fn get_commands(input_str: String) -> Vec<(String, i32)> {
    input_str
        .split("\n")
        .filter(|command| command.trim().len() > 1)
        .map(|command| {
            let command = command.split(" ").collect::<Vec<&str>>();
            // (direction, units)
            (
                command[0].to_string(),
                command[1].parse::<i32>().ok().unwrap(),
            )
        })
        .collect::<Vec<(String, i32)>>()
}

pub fn position(input_str: String) -> i32 {
    let commands = get_commands(input_str);
    let (mut horizontal, mut depth) = (0, 0);

    for (direction, units) in commands {
        match direction.as_str() {
            "forward" => horizontal += units,
            "up" => depth -= units,
            "down" => depth += units,
            _ => {}
        }
    }

    horizontal * depth
}

pub fn position_part02(input_str: String) -> i32 {
    let commands = get_commands(input_str);

    let (mut horizontal, mut depth, mut aim) = (0, 0, 0);

    for (direction, units) in commands {
        match direction.as_str() {
            "forward" => {
                horizontal += units;
                depth += aim * units
            }
            "up" => aim -= units,
            "down" => aim += units,
            _ => {}
        }
    }

    horizontal * depth
}
