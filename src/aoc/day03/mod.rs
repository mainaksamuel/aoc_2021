// Day 03.

use std::cmp::Ordering;

fn input_to_vec(input: &str) -> Vec<String> {
    input
        .split("\n")
        .filter(|line| line.trim().len() > 1)
        .map(|elem| elem.to_string())
        .collect::<Vec<String>>()
}

fn get_binary_stats_at_index(input: &Vec<String>, index: usize) -> (usize, usize) {
    let (mut zeros, mut ones) = (0, 0);
    for bin_number in input {
        match bin_number.chars().nth(index).unwrap_or_default() {
            '0' => zeros += 1,
            '1' => ones += 1,
            _ => {}
        }
    }

    (zeros, ones)
}

fn get_input_binary_stats(input: Vec<String>) -> (Vec<usize>, Vec<usize>) {
    let mut zero_stats = Vec::<usize>::new(); //vec![0; len];
    let mut one_stats = Vec::<usize>::new(); // vec![0; len];

    for i in 0..input[0].len() {
        let (zero, one) = get_binary_stats_at_index(&input, i);
        zero_stats.push(zero);
        one_stats.push(one);
    }

    (zero_stats, one_stats)
}

fn transpose_input(input: String) -> (Vec<usize>, Vec<usize>) {
    let input = input_to_vec(&input);
    get_input_binary_stats(input)
}

pub fn power_consumption(input_str: String) -> i32 {
    let (zeros, ones) = transpose_input(input_str);

    let mut gamma_rate = String::new(); // most common
    let mut epsilon_rate = String::new(); // least common

    for (zero, one) in zeros.iter().zip(ones.iter()) {
        match zero.cmp(one) {
            Ordering::Less => {
                epsilon_rate.push('0');
                gamma_rate.push('1');
            }
            Ordering::Greater => {
                epsilon_rate.push('1');
                gamma_rate.push('0');
            }
            Ordering::Equal => {}
        }
    }

    match (
        i32::from_str_radix(&gamma_rate, 2),
        i32::from_str_radix(&epsilon_rate, 2),
    ) {
        (Ok(gamma), Ok(epsilon)) => gamma * epsilon,
        _ => 0,
    }
}

fn get_supprt_rate(input: &Vec<String>, most_common: bool) -> i32 {
    let mut input = input.to_owned();
    let mut count = input.len();
    let mut index: usize = 0;

    let chr = vec!['1', '0', '1'];
    let idx = if most_common { 0 } else { 1 };

    while count > 1 {
        let (zeros, ones) = get_binary_stats_at_index(&input, index);
        match zeros.cmp(&ones) {
            Ordering::Less => {
                input = input
                    .iter()
                    .filter(|elem| elem.chars().nth(index).unwrap_or_default() == chr[idx])
                    .map(|elem| elem.to_owned())
                    .collect::<Vec<String>>();
            }
            Ordering::Greater => {
                input = input
                    .iter()
                    .filter(|elem| elem.chars().nth(index).unwrap_or_default() == chr[idx + 1])
                    .map(|elem| elem.to_owned())
                    .collect::<Vec<String>>();
            }
            Ordering::Equal => {
                input = input
                    .iter()
                    .filter(|elem| elem.chars().nth(index).unwrap_or_default() == chr[idx])
                    .map(|elem| elem.to_owned())
                    .collect::<Vec<String>>();
            }
        }
        count = input.len();
        index += 1;
    }

    let input_binary = input.join("");
    match i32::from_str_radix(&input_binary, 2) {
        Ok(rate) => rate,
        _ => 0,
    }
}

pub fn power_consumption_part02(input_str: String) -> i32 {
    let input = input_to_vec(&input_str);

    let o2_rate = get_supprt_rate(&input, true); // most common
    let co2_rate = get_supprt_rate(&input, false); // least common

    o2_rate * co2_rate
}
