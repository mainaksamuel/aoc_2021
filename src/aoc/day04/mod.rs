// Day 04.

use std::collections::HashSet;
use std::str::FromStr;

#[derive(Debug, Default, Clone)]
struct Grid {
    numbers: Vec<Vec<i32>>,
    marked: Option<HashSet<(usize, usize)>>,
    won: bool,
    sum_of_unmarked: i32,
}

impl Grid {
    fn new(grid: Vec<Vec<i32>>) -> Self {
        Self {
            numbers: grid,
            marked: None,
            won: false,
            sum_of_unmarked: 0,
        }
    }
    fn mark(&mut self, position: (usize, usize)) {
        match &mut self.marked {
            Some(set) => set.insert(position),
            None => {
                self.marked = Some(HashSet::<(usize, usize)>::new());
                self.marked.as_mut().unwrap().insert(position)
            }
        };
        if self.marked.as_ref().unwrap().len() >= 5 {
            // Start checking for win when marked numbers are at a minimum (i.e. 5) for a win
            self.update_win()
        }
    }

    fn update_win(&mut self) {
        let winning_options = [
            [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4)],
            [(1, 0), (1, 1), (1, 2), (1, 3), (1, 4)],
            [(2, 0), (2, 1), (2, 2), (2, 3), (2, 4)],
            [(3, 0), (3, 1), (3, 2), (3, 3), (3, 4)],
            [(4, 0), (4, 1), (4, 2), (4, 3), (4, 4)],
            [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0)],
            [(0, 1), (1, 1), (2, 1), (3, 1), (4, 1)],
            [(0, 2), (1, 2), (2, 2), (3, 2), (4, 2)],
            [(0, 3), (1, 3), (2, 3), (3, 3), (4, 3)],
            [(0, 4), (1, 4), (2, 4), (3, 4), (4, 4)],
        ];

        if self.marked.is_some() {
            for option in winning_options {
                let mut count = 0;

                for position in option {
                    if self.marked.as_ref().unwrap().contains(&position) {
                        count += 1;
                    }
                }
                if count == 5 {
                    if !self.won {
                        self.won = true;
                        self.update_sum_of_unmarked();
                    }
                    break;
                }
            }
        }
    }

    fn update_sum_of_unmarked(&mut self) {
        let mut sum = 0;
        if self.won {
            for i in 0..self.numbers.len() {
                for j in 0..self.numbers[0].len() {
                    if !self.marked.as_ref().unwrap().contains(&(i, j)) {
                        sum += self.numbers[i][j]
                    }
                }
            }
        }
        self.sum_of_unmarked = sum;
    }
}

impl PartialEq for Grid {
    fn eq(&self, other: &Self) -> bool {
        self.numbers == other.numbers
    }
}

fn get_random_numbers(input: &String) -> Vec<i32> {
    input
        .split("\n")
        .filter(|line| line.trim().len() > 0)
        .nth(0)
        .unwrap()
        .split(",")
        .map(|nums| i32::from_str(nums).unwrap_or_default())
        .collect()
}

fn get_grids(input: &String) -> Vec<Grid> {
    // A vector of 5 x 5 grids.

    let ungridded: Vec<&str> = input
        .split("\n")
        .filter(|line| line.trim().len() > 0)
        .skip(1)
        .collect();

    let grids_str = ungridded
        .chunks_exact(5)
        .map(|row| row.to_vec())
        .collect::<Vec<Vec<&str>>>();

    grids_str
        .iter()
        .map(|grid| {
            grid.iter()
                .map(|nums| {
                    nums.split_whitespace()
                        .map(|n| i32::from_str(n.trim()).unwrap_or_default())
                        .collect::<Vec<i32>>()
                })
                .collect::<Vec<Vec<i32>>>()
        })
        .map(|grid| Grid::new(grid))
        .collect::<Vec<Grid>>()
}

fn find_random_in_grid(grid: &Grid, random: i32) -> Option<(usize, usize)> {
    let mut position = None;
    for i in 0..grid.numbers.len() {
        if let Some(j) = grid.numbers[i].iter().position(|&num| num == random) {
            position = Some((i, j));
            break;
        }
    }

    position
}

pub fn bingo_subsystem(input_str: String) -> i32 {
    let random_numbers: Vec<i32> = get_random_numbers(&input_str);

    let mut score: i32 = 0;
    let mut grids = get_grids(&input_str);

    'outer: for random in random_numbers {
        for grid in &mut grids {
            if let Some(position) = find_random_in_grid(grid, random) {
                grid.mark(position)
            }
            if grid.won {
                score = random * grid.sum_of_unmarked;
                break 'outer;
            }
        }
    }

    score
}

pub fn bingo_subsystem_part02(input_str: String) -> i32 {
    let random_numbers: Vec<i32> = get_random_numbers(&input_str);

    let mut score: i32 = 0;
    let mut grids = get_grids(&input_str);
    let mut winners = Vec::<Grid>::new();

    for random in random_numbers {
        for grid in &mut grids {
            if let Some(position) = find_random_in_grid(grid, random) {
                grid.mark(position)
            }
            if grid.won && !winners.contains(grid) {
                score = random * grid.sum_of_unmarked;
                winners.push(grid.to_owned());
            }
        }
    }

    score
}
