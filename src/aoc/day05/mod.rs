// Day 05.

use std::str::FromStr;

#[derive(Debug, Default, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
}

#[derive(Debug, Clone, Copy)]
struct Line {
    point_a: Point,
    point_b: Point,
    is_diagonal: bool,
}
impl Line {
    fn new(point_a: Point, point_b: Point) -> Self {
        Self {
            point_a,
            point_b,
            is_diagonal: true,
        }
    }

    fn update_diagonal(&mut self) {
        if (self.point_a.x == self.point_b.x) || (self.point_a.y == self.point_b.y) {
            self.is_diagonal = false;
        }
    }
}

fn get_lines(input: String) -> Vec<Line> {
    input
        .split("\n")
        .filter(|n| n.trim().len() > 0)
        .map(|line| line.split(" -> ").collect::<Vec<&str>>())
        .map(|pairs| {
            pairs
                .iter()
                .map(|point| {
                    point
                        .split(",")
                        .map(|num| i32::from_str(num.trim()).unwrap_or_default())
                        .collect::<Vec<i32>>()
                })
                .map(|point| Point::new(point[0], point[1]))
                .collect::<Vec<Point>>()
        })
        .map(|line| Line::new(line[0], line[1]))
        .collect::<Vec<Line>>()
}

fn draw_line(board: &mut Vec<Vec<i32>>, line: &Line) {
    let n = std::cmp::max(
        (line.point_b.x - line.point_a.x).abs() + 1,
        (line.point_b.y - line.point_a.y).abs() + 1,
    );
    let mut x: i32;
    let mut y: i32;
    for i in 0..n {
        x = line.point_a.x + (line.point_b.x - line.point_a.x).signum() * i;
        y = line.point_a.y + (line.point_b.y - line.point_a.y).signum() * i;
        board[y as usize][x as usize] += 1;
    }
}

fn display_board(board: &Vec<Vec<i32>>) {
    for i in 0..board.len() {
        for j in 0..board[0].len() {
            if board[i][j] == 0 {
                print!(". ")
            } else {
                print!("{} ", board[i][j])
            }
        }
        println!()
    }
}

fn check_overlaps(board: &Vec<Vec<i32>>) -> i32 {
    let mut overlaps = 0;
    for x in 0..board.len() {
        for y in 0..board[0].len() {
            if board[x][y] > 1 {
                overlaps += 1;
            }
        }
    }

    overlaps
}

pub fn overlapping_points(input_str: String) -> i32 {
    let mut lines = get_lines(input_str);

    lines.iter_mut().for_each(|line| line.update_diagonal());

    let non_diagonal_lines = lines
        .iter()
        .filter(|line| !line.is_diagonal)
        .collect::<Vec<&Line>>();

    let mut board = vec![vec![0; 1000]; 1000];

    for line in non_diagonal_lines {
        draw_line(&mut board, &line)
    }

    // display_board(&board);

    check_overlaps(&board)
}

pub fn overlapping_points_part02(input_str: String) -> i32 {
    let lines = get_lines(input_str);

    let mut board = vec![vec![0; 1000]; 1000];

    for line in lines {
        draw_line(&mut board, &line)
    }
    // display_board(&board);

    check_overlaps(&board)
}
