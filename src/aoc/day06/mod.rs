// Day 06.

use std::str::FromStr;

fn parse_input(input: String) -> Vec<i32> {
    input
        .trim()
        .split(",")
        .map(|num| i32::from_str(num).unwrap_or_default())
        .collect::<Vec<i32>>()
}

fn simulate(input: &mut Vec<i32>, days: usize) -> i32 {
    for _day in 1..=days {
        let mut zeros = Vec::<i32>::new();
        input.iter().for_each(|timer| {
            if *timer == 0 {
                zeros.push(8)
            }
        });

        input.iter_mut().for_each(|timer| {
            if *timer == 0 {
                *timer = 6
            } else if *timer != 0 {
                *timer -= 1
            }
        });

        if zeros.len() > 0 {
            input.append(&mut zeros)
        }
    }

    input.len() as i32
}

pub fn simulate_lanternfish(input_str: String) -> i32 {
    let mut input = parse_input(input_str);

    simulate(&mut input, 80)
}
