#![allow(dead_code)]
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

mod aoc;
use aoc::*;

fn get_filepath(day: usize, filename: &str) -> String {
    let curr_dir = std::env::current_dir();
    match curr_dir {
        Ok(pwd) => {
            let pwd = pwd.to_str().unwrap();
            if day < 10 {
                format!("{}/src/aoc/day0{}/{}", pwd, day, filename)
            } else {
                format!("{}/src/aoc/day{}/{}", pwd, day, filename)
            }
        }
        Err(reason) => {
            format!("ERROR : {}", reason)
        }
    }
}

fn read_input_file(filepath: &str) -> String {
    let file_input = File::open(filepath);
    let mut contents = String::new();
    match file_input {
        Ok(file) => {
            let mut bufreader = BufReader::new(file);
            match bufreader.read_to_string(&mut contents) {
                Ok(_data) => (),
                Err(reason) => println!(
                    "ERROR: could not read into file `{}`. REASON: {}",
                    filepath, reason
                ),
            }
        }
        Err(reason) => {
            println!(
                "ERROR: Could not find `{}`. Please check the filepath and try again. REASON: {}",
                filepath, reason
            )
        }
    }

    contents
}

fn main() {
    println!("-------------------- Part 1 --------------------");
    let sample = get_filepath(6, "sample.txt");
    let sample_string = read_input_file(&sample);
    println!(
        "Lanternfish (SAMPLE) : {}",
        day06::simulate_lanternfish(sample_string)
    );

    let input = get_filepath(6, "input.txt");
    let input_string = read_input_file(&input);
    println!(
        "Lanternfish (INPUT) : {}",
        day06::simulate_lanternfish(input_string)
    );

    println!("-------------------- Part 2 --------------------");

    // let sample = get_filepath(6, "sample.txt");
    // let sample_string = read_input_file(&sample);
    // println!(
    //     "Lanternfish (SAMPLE) : {}",
    //     day06::simulate_lanternfish_part02(sample_string)
    // );

    // let input = get_filepath(5, "input.txt");
    // let input_string = read_input_file(&input);
    // println!(
    //     "Overlapping (INPUT) : {}",
    //     day05::overlapping_points_part02(input_string)
    // );

    println!("------------------------------------------------");
}
